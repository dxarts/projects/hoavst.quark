+ CtkNode {

	nodeID {
		^this.node
	}

	onFree { arg func;
		var f = {|n, m|
			if(m == \n_end) {
				func.value(this, m);
				this.removeDependant(f);
			}
		};
		// this.register;
		this.addDependant(f);
	}
}