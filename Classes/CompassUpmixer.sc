CompassUpmixer {
	var inputPath, outputPath, outputOrder, balance, inputFormat, outputFormat, inputRefRadius, outputRefRadius, sampleRate;
	var path, files, score, buffer, synth, <vst;
	var inputOrder, numChannels, numOutputChannels;
	var server, note;

	*new { |inputPath, outputPath, outputOrder = 3, balance = 0.5, inputFormat = (['acn', 'n3d']), outputFormat = (['acn', 'n3d']), inputRefRadius = inf, outputRefRadius = (AtkHoa.refRadius), sampleRate = 48000|
		^super.newCopyArgs(inputPath, outputPath, outputOrder, balance, inputFormat, outputFormat, inputRefRadius, outputRefRadius, sampleRate).init
	}

	init {

		server = Server.default;

		path = PathName.new(inputPath);

		numOutputChannels = outputOrder.asHoaOrder.size;

		outputPath.mkdir;

		files = path.isFolder.if({ path.files }, { [path] });

		files.do{ |file|

			server.newAllocators;

			score = CtkScore.new;

			buffer = CtkBuffer(file.fullPath).addTo(score);

			numChannels = buffer.numChannels;

			vst = HoaVST.new('compass_upmixer', 1, numOutputChannels, automateParams: true);

			inputOrder = (numChannels.sqrt - 1).asInteger;

			synth = CtkSynthDef('source', { |buffer, out|
				var in, decode;
				in = PlayBuf.ar(numChannels, buffer, BufRateScale.kr(buffer));
				in = HoaEncodeMatrix.ar(in, HoaMatrixEncoder.newFormat(inputFormat, inputOrder));

				in = case
				// Encode to ATK, apply DIST filter
				{ (inputRefRadius == inf) and: (outputRefRadius == AtkHoa.refRadius) } {
					HoaNFDist.ar(in, inputOrder)
				}
				// decode from ATK, apply Prox filter
				{ (inputRefRadius == AtkHoa.refRadius) and: (outputRefRadius == inf) } {
					HoaNFProx.ar(in, inputOrder)
				}
				// Don't encode to ATK and no change
				{ inputRefRadius == outputRefRadius } { in }
				// input is infinty, rescale to 10 m
				{ (inputRefRadius == inf) and: (outputRefRadius == inf).not} {
					HoaNFCtrl.ar(in, outputRefRadius, 10, inputOrder)
				}
				// output is infinty, rescale to 10 m
				{ (inputRefRadius == inf).not and: (outputRefRadius == inf)} {
					HoaNFCtrl.ar(in, outputRefRadius, 10, inputOrder)
				}
				// default case, both in and out are valid unique radii not inf
				{ HoaNFCtrl.ar(in, outputRefRadius, inputRefRadius, inputOrder) };

				in = vst.ar(in).poll;

				in = HoaDecodeMatrix.ar(in, HoaMatrixDecoder.newFormat(outputFormat, outputOrder));

				Out.ar(out, in)
			});

			note = synth.note(0.0, buffer.duration).buffer_(buffer).out_(0).addTo(score);

			vst.pluginController(note, synth);

			vst.openMsg.do(_.addTo(score));

			vst.inputorder_((inputOrder - 1)/2);
			vst.outputorder_((outputOrder - 1)/6);
			vst.normtype_(0); // n3d
			vst.balance_(balance);

			score.write(
				path: outputPath ++ file.fileNameWithoutExtension ++ "_HOA" ++ outputOrder ++ "_Atk.wav",
				sampleRate: sampleRate,
				sampleFormat: 'float',
				headerFormat: 'WAV',
				options: ServerOptions.new.numOutputBusChannels_(numOutputChannels).blockSize_(1024),
				action: {|err| ("RESULT = " ++ err).postln}
			)
		}
	}
}
