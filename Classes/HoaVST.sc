HoaVST {
	var <plugin, <numVSTs, <vstChannels, <tDesign, <automateParams, <initParams, <reloadPlugins, useParamProtection;
	var <vstPCs, <note;
	var <parameters;
	var <synthParams, params, paramUID;
	var vstIDs, synthDefParams;
	var cond, <decoder, <encoder, <order;
	var <automateFunc, <curveMins, <curveMaxs;
	var <>parameterCurves;

	*new { |plugin, numVSTs, vstChannels, tDesign, automateParams, initParams, reloadPlugins = false, useParamProtection = true|
		^super.newCopyArgs(plugin, numVSTs, vstChannels, tDesign, automateParams, initParams, reloadPlugins, useParamProtection).init
	}

	*archiveParameters { |plugin, presetPath, archivePath|
		var tempServer, thisVST, cond, defaultLatency;
		var synth, note;
		cond = Condition.new;
		defaultLatency = CtkObj.latency;
		CtkObj.latency_(nil);
		tempServer = Server.default;
		tempServer.waitForBoot({
			thisVST = HoaVST.new(plugin);
			synth = CtkSynthDef('tmp', {VSTPlugin.ar(PinkNoise.ar)});
			note = synth.note.play;
			tempServer.sync;
			thisVST.pluginController(note, synth);
			thisVST.open({ cond.test_(true).signal });
			cond.wait;
			cond.test_(false);
			thisVST.vstPCs[0].readProgram(presetPath, {cond.test_(true).signal});
			cond.wait;
			thisVST.writeParams(archivePath);
			CtkObj.latency_(defaultLatency);
			note.free;
		})
	}

	*generateParameterCurves { |plugin, path|
		var pairs, envs, min, max;
		var tempServer, thisVST, cond, defaultLatency;
		var synth, note;
		cond = Condition.new;
		defaultLatency = CtkObj.latency;
		CtkObj.latency_(nil);
		tempServer = Server.default;

		tempServer.waitForBoot({
			thisVST = HoaVST.new(plugin, 1, 1, automateParams: false);
			min = Array.fill(thisVST.parameters.size, { 0.0 });
			max = Array.fill(thisVST.parameters.size, { 0.0 });
			synth = CtkSynthDef('tmp', {thisVST.ar(PinkNoise.ar)});
			note = synth.note.play;
			tempServer.sync;
			thisVST.pluginController(note, synth);
			thisVST.open({ cond.test_(true).signal });
			cond.wait;
			cond.test_(false);
			pairs = 1001.collect{ |i|
				((i/10).asString + "%").postln;
				thisVST.parameters.collect{ |paramName, k|
					thisVST.set(k, i/1000);
					0.012.wait;
					(i == 0).if({ min[k] = thisVST.vstPCs[0].parameterCache[k][1].split($ )[0].asFloat.postln });
					(i == 1000).if({ max[k] = thisVST.vstPCs[0].parameterCache[k][1].split($ )[0].asFloat });
					[thisVST.vstPCs[0].parameterCache[k][1].split($ )[0].asFloat - min[k], i/1000]
				};

			};

			envs = pairs.flop.collect{ |pairs, i|
				Env.pairs(pairs).offset_(min[i])
			};
			CtkObj.latency_(defaultLatency);
			[envs, min, max].writeArchive(path);
			"done".postln;
		})

	}

	*search { |loadCondition|
		Server.default.waitForBoot({
			VSTPlugin.search(action: { loadCondition !? { loadCondition.test_(true).signal }})
		})
	}

	init {

		cond = Condition.new;

		// default args
		numVSTs = numVSTs ?? { 1 };

		vstChannels = vstChannels ?? { 2 };

		vstIDs = numVSTs.collect{ |i| UniqueID.next.asSymbol };

		automateParams = automateParams ?? { true };

		order = ((numVSTs * vstChannels).sqrt - 1).asInteger;

		tDesign.notNil.if({
			encoder = HoaMatrixEncoder.newSphericalDesign(tDesign, order: order);
			decoder = HoaMatrixDecoder.newSphericalDesign(tDesign, order: order);
		});

		// if plugin cache has not been created
		(VSTPlugin.readPlugins.isEmpty or: reloadPlugins).if({
			{
				HoaVST.search(cond);
				cond.wait;
				cond.test_(false);
				this.parseParameters;
				this.parseInitParams;
			}.forkIfNeeded
		}, {
			this.parseParameters;
			this.parseInitParams;
		});

		// add Unique id in case more than one vst is used in the same synth and they have the same parameter names
		paramUID = useParamProtection.if({ UniqueID.next.asSymbol }, { '' });
		automateFunc = FunctionList.new

	}

	pluginController { |aNote, aSynth|
		note = aNote;
		vstPCs = vstIDs.collect{ |id|
			VSTPluginController(note, id, aSynth.synthdef)
		};
		vstPCs[0].addDependant(this);
		vstPCs[0].parameterAutomated_(automateFunc)
	}

	addFunc { |newFunc| automateFunc.addFunc(newFunc) }
	removeFunc { |oldFunc| automateFunc.removeFunc(oldFunc) }

	ar { |in, paramsExtra, lagTime=0.01|
		automateParams = automateParams ++ this.prepareSynthParams(paramsExtra);
		synthDefParams = parameterCurves.notNil.if({
			automateParams.collect{ |param|
				[param[0], IEnvGen.kr(parameterCurves[param[0]], Lag.kr((param[1] ++ paramUID).asSymbol.kr(parameterCurves[param[0]].atLevel(params[param[0]])[0]), lagTime))]
			}.flat
		}, {
			automateParams.collect{ |param|
				[param[0], Lag.kr((param[1] ++ paramUID).asSymbol.kr(params[param[0]]), lagTime)]
			}.flat
		});
		tDesign.notNil.if({ in = HoaDecodeMatrix.ar(in, decoder) });
		in = vstIDs.collect{ |id, i|
			var src;
			src = case
			{ (vstChannels == 1) and: (vstIDs.size == 1) } { in }
			{ vstChannels == 1 } { in[i] }
			{ in[vstChannels*i..vstChannels*(i+1)-1] };
			synthDefParams.isEmpty.not.if({
				// run the VSTPlugin
				VSTPlugin.ar(src,
					vstChannels, id: id,
					params: synthDefParams
				);
			}, {
				VSTPlugin.ar(src, vstChannels, id: id);
			})
		}.flat;
		^tDesign.notNil.if({ in = HoaEncodeMatrix.ar(in, encoder) }, { in })

	}

	open { |action, verbose = false, editor = true|
		vstPCs.do{ |thisVST| thisVST.open(plugin, editor, verbose, action) }
	}

	openMsg { |time = 0.0|
		^vstPCs.collect{ |thisVSTPC| CtkMsg.new(nil, time, thisVSTPC.openMsg(plugin)) }
	}

	loadPreset { |preset, action, async = false|
		vstPCs.do{ |thisVST| thisVST.loadPreset(preset, action, async) }
	}

	searchMsg { |time = 0.0|
		^CtkMsg.new(nil, time, VSTPlugin.searchMsg)
	}

	loadParameterCurves { |path|
		#parameterCurves, curveMins, curveMaxs = Object.readArchive(path)
	}

	presetMsg { |path, time = 0.0|
		var thisPath;
		path.isNil.if({
			initParams.isKindOf(String).not.if({
				"Please supply a path to a preset file".warn;
				^[]
			}, {
				thisPath = initParams
			})
		}, {
			thisPath = path
		});
		^vstPCs.collect{ |thisVSTPC| CtkMsg(nil, time, thisVSTPC.readProgramMsg(thisPath)) };
	}

	parseParameters {

		parameters = VSTPlugin.readPlugins[plugin].parameters
		.collect(_.name)
		.collect(_.asSymbol);

		synthParams = VSTPlugin.readPlugins[plugin].parameters
		.collect(_.name)
		.collect(_.replace(" ", ""))
		.collect(_.replace("/", ""))
		.collect(_.replace(":", ""))
		.collect(_.replace(".", ""))
		.collect(_.capsToSpaces(""))
		.collect{ |str| str[0].toLower.asString ++ str[1..]};

		synthParams.do{ |param|

			while ({param.contains("(") and: param.contains(")")}, {
				var first, last;
				first = param.find("(");
				last = param.find(")");
				(last - first + 1).do{
					param.removeAt(first)
				}
			})

		};

		synthParams = synthParams
		.collect(_.replace("("))
		.collect(_.replace(")"))
		.collect(_.replace("_"))
		.collect(_.replace("-"))
		.collect(_.asSymbol);

		automateParams = this.prepareSynthParams(automateParams)

	}

	prepareSynthParams { |automateParams|

		// check supplied automated parameters
		^case
		{ automateParams.isKindOf(Boolean) } {
			// if a boolean and true, set to all parameters
			automateParams.if({
				synthParams.collect{ |param, i|
					[i, param]
				}
			}, {
				[]
			})
		}
		// if array, parse array
		{ automateParams.isKindOf(Array) }
		{
			case
			{ automateParams[0].isKindOf(Integer) } { // array of integers as parameters
				automateParams.collect{ |index|
					[index, synthParams[index]]
				}
			}
			// array of parameter names
			{ automateParams[0].isKindOf(String) or: automateParams[0].isKindOf(Symbol) } {
				automateParams.collect{ |param|
					var index;
					param = param.asSymbol;
					// check if name is in synth parameter format
					synthParams.includes(param).if({
						index = synthParams.indexOf(param);
						[index, synthParams[index]]
					}, {
						// check if name is in original format
						parameters.includes(param).if({
							index = parameters.indexOf(param);
							[index, synthParams[index]]
						}, {
							// parameter name not found
							("Automated parameter" + param + "not found!").warn;
						})
					})
				}
			}
		}
		// default
		{ [] }

	}

	parseInitParams {
		case
		{ initParams.isKindOf(String) } {
			(initParams.extension == "scd").if({ this.loadParams(initParams) })
		} // params is scd
		{ initParams.isKindOf(Array) } { params = initParams }
		{ params = 0.5.dup(parameters.size) };
	}

	// asynchronous
	writeParams { |path|
		vstPCs[0].getn(action: { |val| val.writeArchive(path) });
	}

	loadParams { |initParams, setParams = false|
		initParams.isString.if({ params = initParams.load }, { params = initParams });
		setParams.if({ this.setn });
	}

	checkParam { |paramName|
		^case
		// if it's an integer, return the parameter name, either the automation name or the original name
		{ paramName.isKindOf(Integer) } { paramName }
		// if string or symbol
		{ paramName.isKindOf(String) or: paramName.isKindOf(Symbol) } {
			case
			// if its in the synth param spelling
			{ synthParams.includes(paramName.asSymbol) } {
				synthParams.indexOf(paramName.asSymbol)
			}
			// if its in the orignial spelling
			{ parameters.includes(paramName.asSymbol) } { parameters.indexOf(paramName.asSymbol) }
		}
		{
			Error("Parameter" + paramName + "not found").throw
		}

	}

	checkAutomation { |paramID|
		^automateParams.flop[0].asArray.includes(paramID)
	}

	set { |paramName, paramVal|
		paramName = this.checkParam(paramName);
		this.checkAutomation(paramName).if({
			this.note.perform((synthParams[paramName] ++ paramUID ++ '_').asSymbol.postln, paramVal.postln)
		}, {
			(parameterCurves.notNil and: paramVal.isKindOf(CtkControl).not).if({ paramVal = parameterCurves[paramName][paramVal] });
			paramVal.isKindOf(CtkControl).not.if({
				vstPCs.do{ |thisVST| thisVST.set(paramName, paramVal) }
			}, {
				vstPCs.do{ |thisVST| thisVST.map(paramName, paramVal.bus) }
			})
		})
	}

	setn { |paramNames, paramVals|
		paramVals.isString.if({ this.loadParams(paramVals); paramVals = nil });
		paramVals !? { params = paramVals };
		paramNames = paramNames ?? { parameters };
		paramNames.isKindOf(Array).not.if({ paramNames = [paramNames] });
		params.isKindOf(Array).not.if({ params = [params] });
		paramNames.do{ |paramName, i|
			this.set(paramName, params[i])
		}
	}

	get { |paramName, action|
		paramName = this.checkParam(paramName);
		this.checkAutomation(paramName).if({
			var argVal = this.note.perform((synthParams[paramName] ++ paramUID).asSymbol);
			argVal.isKindOf(CtkControl).if({
				action.value(argVal.getSynchronous)
			}, {
				action.value(argVal)
			})

		}, {
			this.vstPCs[0].get(paramName, { |val| action.value(val) })
		})
	}

	getn { |paramNames, action|
		var cond, res;
		cond = Condition.new;
		paramNames = paramNames ?? { parameters };
		res = Array.fill(paramNames.size, { 0.0 });
		{

			paramNames.do{ |paramName, i|
				this.get(paramName, { |val| res[i] = val; cond.test_(true).signal });
				cond.wait;
				cond.test_(false)
			};
			action.value(res)

		}.fork


	}

	update { arg who, what ...args;

		who.notNil.if {
			var thisParam, thisVal;
			switch(what,
				'automated', {
					// [who, what, what.class, args, args.last.class].postln;
					thisParam = synthParams[args[0]];
					thisVal = parameterCurves.notNil.if({
						parameterCurves[args[0]].atLevel(args[1])[0]
					}, {
						args[1]
					});

					// for now, leave CtkControls alone...
					automateParams.flop[0].asArray.includes(args[0]).if({
						this.note.perform((thisParam ++ paramUID ++ '_').asSymbol, thisVal)
					}, {
						// if not an automated parameter set only the other vstPCs
						vstPCs[1..].do{ |thisVST| thisVST.set(args[0], args[1]) }
					})
				}
			)
		}
	}

	gui { |parent, bounds, params = true|
		vstPCs[0].gui(parent, bounds, params)
	}

	editor { |show = true|
		vstPCs[0].editor(show)
	}

	doesNotUnderstand { |selector, args|
		var set = false, paramName;
		selector = selector.asString;
		(selector.last == $_).if({
			selector = selector[0..selector.size-2];
			set = true;
		});
		selector = selector.asSymbol;
		(args.isNil and: set.not).if({ args = { |val| val.postln } });
		(synthParams.includes(selector) or: parameters.includes(selector)).if({
			set.if({ this.set(selector, args) }, { this.get(selector, args) })
		}, {
			DoesNotUnderstandError(this, selector, args).throw;
		})
	}

	postParameterValues {
		synthParams.do{ |paramName, paramIndex|
			var min = 0.0, max = 1.0;
			parameterCurves.notNil.if({
				min = curveMins[paramIndex];
				max = curveMaxs[paramIndex]
			});
			[paramName, min, max].postln
		}
	}

}


/*

put on VSTPlugin repo issues:
ERROR: PluginBridge::pollUIThread: plugin 1 doesn't exist (anymore)
Is there a way to get the max and min of a particular parameter??

*/



