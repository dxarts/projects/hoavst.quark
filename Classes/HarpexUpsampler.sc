HarpexUpsampler {
	var inputPath, outputPath, envelopment, inputFormat, outputFormat, inputRefRadius, outputRefRadius, sampleRate;
	var path, files, score, buffer, outputOrder, synths, vst;
	var inputOrder, numChannels, numOutputChannels;
	var normType, server;

	*new { |inputPath, outputPath, envelopment = 1.0, inputFormat = (['acn', 'n3d']), outputFormat = (['acn', 'n3d']), inputRefRadius = inf, outputRefRadius = (AtkHoa.refRadius), sampleRate = 48000|
		^super.newCopyArgs(inputPath, outputPath, envelopment, inputFormat, outputFormat, inputRefRadius, outputRefRadius, sampleRate).init
	}

	init {

		server = Server.default;

		outputOrder = 3;

		path = PathName.new(inputPath);

		numOutputChannels = outputOrder.asHoaOrder.size;

		outputPath.mkdir;

		files = path.files;

		files.do{ |file|

			server.newAllocators;

			score = CtkScore.new;

			buffer = CtkBuffer(file.fullPath).addTo(score);

			numChannels = buffer.numChannels;

			vst = HoaVST.new('O3A Harpex Upsampler', 1, numChannels);

			inputOrder = (numChannels.sqrt - 1).asInteger;

			synths = CtkProtoNotes(

				SynthDef('source', { |buffer, out|
					var in, decode;
					in = PlayBuf.ar(numChannels, buffer, BufRateScale.kr(buffer));
					in = HoaEncodeMatrix.ar(in, HoaMatrixEncoder.newFormat(inputFormat, inputOrder));

					in = case
					// Encode to ATK case, apply DIST filter
					{ (inputRefRadius == inf) and: (outputRefRadius == AtkHoa.refRadius) } {
						HoaNFDist.ar(in, inputOrder)
					}
					// decode from ATK, apply Prox filter
					{ (inputRefRadius == AtkHoa.refRadius) and: (outputRefRadius == inf) } {
						HoaNFProx.ar(in, inputOrder)
					}
					// Don't encode to ATK and no change
					{ inputRefRadius == outputRefRadius } { in }
					// input is infinty, rescale to 10 m
					{ (inputRefRadius == inf) and: (outputRefRadius == inf).not} {
						HoaNFCtrl.ar(in, outputRefRadius, 10, inputOrder)
					}
					// output is infinty, rescale to 10 m
					{ (inputRefRadius == inf).not and: (outputRefRadius == inf)} {
						HoaNFCtrl.ar(in, outputRefRadius, 10, inputOrder)
					}
					// default case
					{ HoaNFCtrl.ar(in, outputRefRadius, inputRefRadius, inputOrder) };


					in = HoaDecodeMatrix.ar(in, HoaMatrixDecoder.newFormat(['acn', 'sn3d'], inputOrder));

					vst = vst.ar(in);

					in = HoaEncodeMatrix.ar(in, HoaMatrixEncoder.newFormat('ambix', outputOrder));

					Out.ar(out, HoaDecodeMatrix.ar(in, HoaMatrixDecoder.newFormat(outputFormat, outputOrder)))
				})

			);

			synths['source'].note(0.0, buffer.duration).buffer_(buffer).out_(0).addTo(score);

			vst.openMsg.addTo(score);

			vst.note.envlpmnt_(envelopment/2);
			vst.note.format_(0); // SN3D

			score.write(
				path: outputPath ++ file.fileNameWithoutExtension ++ "_HOA" ++ outputOrder ++ "_Atk.wav",
				sampleRate: sampleRate,
				sampleFormat: 'float',
				headerFormat: 'WAV',
				options: ServerOptions.new.numOutputBusChannels_(numOutputChannels),
				action: {|err| ("RESULT = " ++ err).postln}
			)
		}
	}
}
