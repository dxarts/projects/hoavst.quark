# HoaVST

A SuperCollider quark for using the VSTPlugin with higher-order ambisonics.
- [atk-sc3](https://github.com/ambisonictoolkit/atk-sc3)
- [ATK](http://www.ambisonictoolkit.net/)
- [VSTPlugin](https://git.iem.at/pd/vstplugin/tree/master)
